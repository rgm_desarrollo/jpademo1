/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion_fallas;

import aplicacion_fallas.exceptions.NonexistentEntityException;
import aplicacion_fallas.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jroma
 */
public class FallasJpaController implements Serializable {

    public FallasJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Fallas fallas) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(fallas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findFallas(fallas.getId()) != null) {
                throw new PreexistingEntityException("Fallas " + fallas + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Fallas fallas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            fallas = em.merge(fallas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = fallas.getId();
                if (findFallas(id) == null) {
                    throw new NonexistentEntityException("The fallas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Fallas fallas;
            try {
                fallas = em.getReference(Fallas.class, id);
                fallas.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The fallas with id " + id + " no longer exists.", enfe);
            }
            em.remove(fallas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Fallas> findFallasEntities() {
        return findFallasEntities(true, -1, -1);
    }

    public List<Fallas> findFallasEntities(int maxResults, int firstResult) {
        return findFallasEntities(false, maxResults, firstResult);
    }

    private List<Fallas> findFallasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Fallas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Fallas findFallas(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Fallas.class, id);
        } finally {
            em.close();
        }
    }

    public int getFallasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Fallas> rt = cq.from(Fallas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
