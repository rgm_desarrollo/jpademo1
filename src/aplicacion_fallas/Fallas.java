/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacion_fallas;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jroma
 */
@Entity
@Table(name = "fallas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fallas.findAll", query = "SELECT f FROM Fallas f")
    , @NamedQuery(name = "Fallas.findById", query = "SELECT f FROM Fallas f WHERE f.id = :id")
    , @NamedQuery(name = "Fallas.findByFecha", query = "SELECT f FROM Fallas f WHERE f.fecha = :fecha")
    , @NamedQuery(name = "Fallas.findByDescripcion", query = "SELECT f FROM Fallas f WHERE f.descripcion = :descripcion")
    , @NamedQuery(name = "Fallas.findByArea", query = "SELECT f FROM Fallas f WHERE f.area = :area")
    , @NamedQuery(name = "Fallas.findByUsuario", query = "SELECT f FROM Fallas f WHERE f.usuario = :usuario")
    , @NamedQuery(name = "Fallas.findBySolucion", query = "SELECT f FROM Fallas f WHERE f.solucion = :solucion")})
public class Fallas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "fecha")
    private String fecha;
    @Column(name = "Descripcion")
    private String descripcion;
    @Column(name = "Area")
    private String area;
    @Column(name = "Usuario")
    private String usuario;
    @Column(name = "Solucion")
    private String solucion;

    public Fallas() {
    }

    public Fallas(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSolucion() {
        return solucion;
    }

    public void setSolucion(String solucion) {
        this.solucion = solucion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fallas)) {
            return false;
        }
        Fallas other = (Fallas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aplicacion_fallas.Fallas[ id=" + id + " ]";
    }
    
}
